Array.prototype.binarySearch = function(target) {
	let l = 0,
		h = this.length - 1,
		comparator = (a, b) => (a < b ? -1 : (a > b ? 1 : 0)),
		m, comparison;

	while (l <= h) {
		m = (l + h) >>> 1;
		comparison = comparator(this[m], target);
		if (comparison < 0) {
			l = m + 1;
		} else if (comparison > 0) {
			h = m - 1;
		} else {
			return m;
		}
	}
	return ~l;
};

Array.prototype.binaryInsert = function(target, duplicate) {
	var i = this.binarySearch(target);
	if (i >= 0) {
		if (!duplicate) {
			return i;
		}
	} else {
		i = ~i;
	}
	this.splice(i, 0, target);
	return i;
};


const readline = require('linebyline'),
	rl = readline('./input.txt'),
	sorted_arr = [];



(async () => {
	try {
		rl.on('line', function(line, lineCount, byteCount) {
				// console.log(line, lineCount);
				if (lineCount > 1) {
					sorted_arr.binaryInsert(parseFloat(line));
					// console.log("sorted_arr", sorted_arr);
					console.log(`No.: ${lineCount-1}, Number: ${line}, Median: ${
						sorted_arr.length == 1 ? sorted_arr[0] :
						sorted_arr.length % 2 ?  
							(sorted_arr[(sorted_arr.length-1)/2] + sorted_arr[(sorted_arr.length-1)/2+1])/2: 
							sorted_arr[sorted_arr.length/2]
					}`);
				} else {
					console.log(`Total Numbers: ${line}`);
				}
			})
			.on('error', function(e) {
				throw e;
				// something went wrong 
			});

	} catch (err) {
		console.error(err);
	}
})();